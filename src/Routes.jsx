import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import LoginPage from 'page/login/login'



const Routes = () => {
  return (
    <Switch>
      <Redirect exact from="/" to="/login"></Redirect>
      <Route exact path="/"></Route>
      <Route path="/login" component={LoginPage} />
    </Switch>
  );
}

export default Routes;